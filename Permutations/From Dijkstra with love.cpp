#include <fstream>
#include <iostream>
#define N 100

using namespace std;

void swap(int &a, int &b) {
	int c;
	c = a;
	a = b;
	b = c;
}

void printArray(int *a, int n) {
	for (int i = 0; i < n; ++i)
		cout << a[i] << " ";
	cout << endl;
}

int factorial(int n) {
	if (n == 0)
		return 1;
	return n*factorial(n-1);
}

void nextPermutation(int *p, int n) { //��������� ������������ �� ��������
	int i, j;
	for (i = n - 2; i >= 0; --i) //������ ���
		if (p[i] < p[i + 1]) 
			break;	//����� ������ 
		else if (i == 0)
			return;

	for (j = n - 1; j >= i + 1; --j) //������ ���: ����� � ������ � �������� �������
		if (p[j] > p[i]) {
			swap (p[j], p[i]);
			break;
		}

	for (j = i + 1; j < (n - (n - i) / 2); ++j) //����������� ����� �� �����������
		swap(p[j], p[n - j + i]);
}

void main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n; //������� ������������
	int p[N];
	cin >> n ;

	for (int i = 0; i < n; ++i)
		p[i] = i+1;

	printArray(p, n);

	for (int i = 0; i < factorial(n) - 1; ++i) {
		nextPermutation(p, n);
		printArray(p, n);
	}
}