#include <fstream>
#include <iostream>
#include <vector>
#include <iterator>

using namespace std;

void printVector(vector<int> &a) {
	copy(a.begin(), a.end(), ostream_iterator<int>(cout," "));
}

void permut(vector<int> &source, vector<int> &result) { //�������� � �������������� ������ permut('12345', '');
	int size_source = source.size();
	int size_result = result.size();
	vector<int> p1;
	vector<int> p2;

	if (size_source == 0) {
		printVector(result);
		cout << endl;
	}
	else {
		for (int i = 0; i < size_source; ++i) { //�������� ��������� permut, �� �������� ������ i-��� ������� ��������� � ����� ��������������
			//��������� i-��� ������ � ������ result
			p1 = result;
			p1.push_back(source[i]); //��������� � ����� �������
			//������� i-��� ������ �� ������ source
			p2 = source;
			p2.erase(p2.begin() + i);
			//����������� �������
			permut(p2, p1);
		}
	}
}

void main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n; //������� ������������
	cin >> n ;
	vector<int> p1(n); //�������� ������
	vector<int> p2; //���������� ������ �������������� ������

	for (int i = 0; i < n; ++i) 
		p1[i] = i+1;

	permut(p1, p2);
}