#include <fstream>
#include <iostream>
#include <vector>
#include <iterator>

using namespace std;

void printVector(vector<int> &a) {
	copy(a.begin(), a.end(), ostream_iterator<int>(cout," "));
}

int factorial(int n) {
	if (n == 0)
		return 1;
	return n*factorial(n-1);
}

void showPermutation(vector<int> &b) { //�� ������� �������� ����� � ������������
	cout << "Invers: ";
	printVector(b);
	int n = b.size();
	vector<int> p(n); //������ ������������
	for (int i = 1; i <= n; ++i) {
		int k = 0, j = 0; //k - ���������� ����, j - ������
		while (k <= b[i - 1]) { //���������� b[i - 1] ������ ����
			if (p[j] == 0) //���� ����� ������ �����
				++k;
			++j;
		}
		p[j - 1] = i; //�������� i �� ��������� ������ �����, ������ ��� ������ ��� ����������� ��������� 
	}
	cout << "  Permut: ";
	printVector(p);
	cout << endl;
}

void nextInversionTable(vector<int> &b) { //���������� ������� ��������
	int n = b.size();
	int carry = 1; //���� �������� (��, ��� �� ����������, ����� ��������� � ����. ������ �����)
	for (int i = n - 1; i >= 0; --i) {
		b[i] += carry;
		carry = 0;
		if (b[i] > n - i - 1) {	
			b[i] = 0;
			carry = 1;
		}
	}
}

void main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n; //������� ������������
	int i;
	cin >> n;
	vector<int> b(n); //����������� ������ ��� n ��������� ���� int
					  //������� ��������, ��������� �� �����
	showPermutation(b);
	for (i = 0; i < factorial(n) - 1; ++i) {
		nextInversionTable(b);
		showPermutation(b);
	}
}