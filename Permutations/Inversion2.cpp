#include <fstream>
#include <iostream>
#define N 100

using namespace std;

void printArray(int *a, int n) {
	for (int i = 0; i < n; ++i)
		cout << a[i] << " ";
}

int factorial(int n) {
	if (n == 0)
		return 1;
	return n*factorial(n-1);
}

void showPermutation(int *b, int n) { //�� ������� �������� ����� � ������������
	cout << "Invers: ";
	printArray(b, n);
	int p[N];
	for (int i = 0; i < n; ++i)
		p[i] = 0;
	for (int i = 1; i <= n; ++i) {
		int k = 0, j = 0; //k - ���������� ����, j - ������
		while (k <= b[i - 1]) { //���������� b[i - 1] ������ ����
			if (p[j] == 0) //���� ����� ������ �����
				++k;
			++j;
		}
		p[j - 1] = i; //�������� i �� ��������� ������ �����, ������ ��� ������ ��� ����������� ��������� 
	}
	cout << "  Permut: ";
	printArray(p, n);
	cout << endl;
}

void nextInversionTable(int *b, int n) { //���������� ������� ��������
	int carry = 1; //���� �������� (��, ��� �� ����������, ����� ��������� � ����. ������ �����)
	for (int i = n - 1; i >= 0; --i) {
		b[i] += carry;
		carry = 0;
		if (b[i] > n - i - 1) {	
			b[i] = 0;
			carry = 1;
		}
	}
}

void main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n; //������� ������������
	cin >> n;
	int b[N]; //������� ��������, ��������� �� �����
	for (int i = 0; i < n; ++i)
		b[i] = 0;
	showPermutation(b, n);
	for (int i = 0; i < factorial(n) - 1; ++i) {
		nextInversionTable(b, n);
		showPermutation(b, n);
	}
}