// �������� ������� ������������������ ��������������� �� ������, � ������ ����� ����������� ������� ����������� 
// � ���������� ����� ����� ����� ������������� ���������
#include <fstream>
#include <iostream>
#include <vector>
#include <iterator>

using namespace std;

void printVector(vector<int> &a) {
	copy(a.begin(), a.end(), ostream_iterator<int>(cout," "));
}

void swap(vector<int> &a, int i, int j) {
	int x; //��������������� ����������
	x = a[i];
	a[i] = a[j];
	a[j] = x;
}

void sort(vector<int> &a) { //�������� ������ �� ����. � ������. �����. � ������. ������� ��������� 1 �������
	for (int i = 1; i < a.size(); ++i) { //i - ����� 1�� �������� � ������. ����� �������
		int x = a[i];
		int j = i - 1;
		//��� �������� ������. �����, �������, ��� x, �������� �� ������� ������:
		while ((j >= 0) && (a[j] > x)) {
			a[j + 1] = a[j];
			--j;
		}
		a[j + 1] = x; //������� x ������ �� ��� ����� �� �������
	}
}

//{
//	for(int i = 1; i < n; i++)     
//		for(int j = i; (j > 0) && (x[j - 1] > x[j]); j--) // ���� j > 0 � ������� j - 1 > j, x - ������ int
//				swap(x[j - 1], x[j]);        // ������ ������� �������� j � j-1
//}

void main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n;
	cin >> n;
	vector<int> a(n);
	for (int i = 0; i < a.size(); ++i) {
		a[i] = 1 + rand() % 100;
	}
	printVector(a);
	cout << endl;
	sort(a);
	printVector(a);

}