#include <fstream>
#include <iostream>

void mergeFiles(std::ifstream &fin1, std::ifstream &fin2, std::ofstream &fout) 
 {
	int x, y;
	bool fail1 = (fin1 >> x).fail(); //��������� �� ������ ������
    bool fail2 = (fin2 >> y).fail();                            
    while (!fail1 || !fail2) //���� �� ����� ������
    {
        if (!fail1 && !fail2)
        {
           if (x < y)
           {
              fout << x << " ";
              fail1 = (fin1 >> x).fail();
           }
		   else
           {
              fout << y << " ";
              fail2 = (fin2 >> y).fail();
           }
        } 
		else if (!fail1)
        {
           fout << x << " ";
           fail1 = (fin1 >> x).fail();
        } 
		else if (!fail2)
        {
            fout << y << " ";
            fail2 = (fin2 >> y).fail();        
        }
    }
	fin1.close();
    fin2.close();
    fout.close();
 }
void main()
{
    std::ifstream ffirst("f1.txt"), fsecond("f2.txt");
    std::ofstream fout("output.txt");
	mergeFiles(ffirst, fsecond, fout);
}