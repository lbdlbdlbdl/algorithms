//1. �� ������� ���������� ��������� ������� ������� a[i]
//2. ����������� ��������� ���������� �������, ������� ���������� ��� ����� <= a[i], ����� �� ����, � ��� ����� >= a[i] - ������
//������ ������ ������� �� ���� �����������, ������ ����� ������, ���� ����� �������,
//3. ��� ����� �����������: ���� � ���������� ����� ���� ���������, ���������� ��������� ��� ���� �� �� ���������.
#include <fstream>
#include <iostream>
#include <vector>
#include <iterator>
#include <ctime>

using namespace std;

void printVector(vector<int> &a) {
	copy(a.begin(), a.end(), ostream_iterator<int>(cout," "));
}

void swap(vector<int> &a, int i, int j) {
	int x; //��������������� ����������
	x = a[i];
	a[i] = a[j];
	a[j] = x;
}

void QuickSort(vector<int> &a, int left, int right) {
	int i = left, j = right;
	int p = (i + j) / 2; //����������� (������������) �������, �����-�� �������� ����-�� ��������� ��-� �������
	//���������� �������:
	while (i <= j) {
		while (a[i] < a[p])
			++i;
		while (a[j] > a[p])
			--j;
		if (i <= j) {
			if (a[i] > a[j])
				swap(a, i, j);
			++i;
			--j;
		}
	}
	//��������� �������� ����� � ������ ����� ������. �������:
	if (i < right)
		QuickSort(a, i, right);
	if (left < j)
		QuickSort(a, left, j);
}

int main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n;
	cin >> n;
	vector<int> a(n);
	srand(time(0));
	for (int i = 0; i < a.size(); ++i) {
		a[i] = 1 + rand() % 100;
	}
	printVector(a);
	cout << endl;
	QuickSort(a, 0, a.size() - 1);
	printVector(a);

}
