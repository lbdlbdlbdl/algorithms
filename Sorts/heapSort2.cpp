#include <fstream>
#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>

using namespace std;
   
void printVector(vector<int> &a) {
	copy(a.begin(), a.end(), ostream_iterator<int>(cout," "));
}

void restruct (vector<int>::iterator tree_begin, vector<int>::iterator tree_end, int i) { //�������� �� ������� �������. ��� ����� ������������ � �������� ��� ������� //vector<int>::iterator it = a.begin() + 5; a[5] == *it;
    vector<int>::iterator parent = tree_begin + i;
    vector<int>::iterator left_child = tree_begin + 2 * i + 1;
    vector<int>::iterator right_child = tree_begin + 2 * i + 2;        
    if (left_child >= tree_end) // ���� ��� ��������
        return;
    else if (right_child == tree_end) // ���� ���� ������ ����� �������
		if (*left_child > *parent) { // ���� ������� ������ ��������
            swap(*left_child, *parent);
            restruct(tree_begin, tree_end, left_child - tree_begin);
        }   
	else { // ���� ���� ��� ������� 
        vector<int>::iterator max_child = *left_child > *right_child ? left_child : right_child; // ������� �������� �������
        if (*max_child > *parent) { // ���� ������� ������ ��������
            swap(*max_child, *parent);
            restruct(tree_begin, tree_end, max_child - tree_begin);
        }
    }
}

int main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	vector<int> a;
	int x;
	while (!cin.eof()) {
          cin >> x;
          a.push_back(x);
    }
    int last = a.size() - 1;  // ���������
    int k = a.size() / 2 - 1; // ��������� � ����� �� ���, � �������� ��� �� 1 �������
							  // ���� �������� ��������, � ������� ���� ���� �� 1 �������. k - 1� �������, � �������� �� ���
	// ����������� ����������� ������:
    for (int i = k - 1; i >= 0; --i)
        restruct(a.begin(), a.end(), i);
	// ������� �������� �� ����� � ������������� ������:
    for (int i = last; i > 0; --i) {
        restruct(a.begin(), a.begin() + i + 1, 0); // + 1, ������ ��� vector::end ��������� �� ������� ����� ���������� ��������
        swap(a[i], a[0]);        
    }
    printVector(a);
	return 0;
}
