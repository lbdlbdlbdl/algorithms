//1. ������� ����� ������������ �������� � ������� ������
//2. ���������� ����� ����� �������� �� ��������� ������ ����������������� ������� (����� �� �����, ���� ����������� ������� ��� ��������� �� ������ �������)
//3. ��������� ����� ������, �������� �� ������������ ��� ��������������� ��������
#include <fstream>
#include <iostream>
#include <vector>
#include <iterator>

using namespace std;

void printVector(vector<int> &a) {
	copy(a.begin(), a.end(), ostream_iterator<int>(cout," "));
}

void swap(vector<int> &a, int i, int j) {
	int x; //��������������� ����������
	x = a[i];
	a[i] = a[j];
	a[j] = x;
}

void sort(vector<int> &a) { //�������� ������ �� ������. � ������. �����
	for (int i = 0; i < a.size() - 1; ++i) {
		int min = i;  //i - ����� 1-�� ��. � ��������. ����� �������. ������� ���� ������ - ��������������� �����
		//������� ���. �� � ������. �����:
		for (int j = i + 1; j < a.size(); ++j) 
			if (a[j] < a[min])
				min = j;
		if (i != min) // ��������� ���. ��. �������� ������� � 1 ��. ������. �����
			swap(a, i, min);
	}
}

void main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n; //����� �������
	cin >> n;
	vector<int> a(n);
	for (int i = 0; i < a.size(); ++i) {
		a[i] = 1 + rand() % 100;
	}
	printVector(a);
	cout << endl;
	sort(a);
	printVector(a);
}