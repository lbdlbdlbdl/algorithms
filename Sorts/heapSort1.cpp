#include <fstream>
#include <iostream>
#include <vector>
#include <iterator>

using namespace std;

void printVector(vector<int> &a) {
	copy(a.begin(), a.end(), ostream_iterator<int>(cout," "));
}

void swap(vector<int> &a, int i, int j) {
	int x = a[i];
	a[i] = a[j];
	a[j] = x;
}

int restruct(vector<int> &a, int i, int n) {
	while (2 * i <= n) {
		int r = 2 * i;
		if ((r + 1 <= n) && (a[r] < a[r+1])) 
			++r;
		if (a[i] < a[r]) {
			swap(a, i, r);
			i = r;
		} 
		else
			return 0;
	}
	return 0;
}

int main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	vector<int> a;
	int i, x;
	while (!cin.eof()) {
		cin >> x;
		a.push_back(x);
	}

	int n = a.size() - 1;
	//���������� ��������:
	i = n / 2;
	while (i >= 0) {
		restruct(a, i, n);
		--i;
	}
	//���������� �� ��������:
	i = n;
	while (i > 0) {
		swap(a, 0, i);
		--i;
		restruct(a, 0, i);
	}
	printVector(a);
}
