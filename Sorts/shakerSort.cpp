//��-������, ���� ��� �������� �� ����� ������� ������������ �� ����������, �� ��� ����� ������� ��� ������������� �, �������������, �� ����� ��������� �� ������������.
//��-������, ��� �������� �� ����� ������� � ������ ����������� ������� ���������� �� ������ �������, � ������������ ������� ���������� ������ �� ���� ������� ������.
#include <fstream>
#include <iostream>
#include <vector>
#include <iterator>
#include <ctime>

using namespace std;

void printVector(vector<int> &a) {
	copy(a.begin(), a.end(), ostream_iterator<int>(cout," "));
}

void swap(vector<int> &a, int i, int j) {
	int x; //��������������� ����������
	x = a[i];
	a[i] = a[j];
	a[j] = x;
}

void cocktailSort(vector<int> &a) 
{
	int n = a.size(), left = 0, right = n - 1;
	bool f = true;
	while ((left < right) && f) 
	{ 
		f = false;
		for (int i = left; i < right; ++i) //��������� ����� �������
		{
			if (a[i] > a[i + 1]) 
			{
				swap(a, i , i + 1);
				f = true;
			}
		}
		--right;
		for (int i = right; i > left; --i)  //��������� ������ ������
		{
			if (a[i] < a[i - 1]) 
			{
				//swap(a, i - 1, i);
				swap(a, i, i - 1);
				f = true;
			}
		}
		++left;
	}
}

void main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n;
	cin >> n;
	vector<int> a(n);
	srand(time(0));
	for (int i = 0; i < a.size(); ++i) 
	{
		a[i] = 1 + rand() % 100;
	}
	printVector(a);
	cout << endl;
	cocktailSort(a);
	printVector(a);
}