#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <utility>

using namespace std;

vector<vector<int> > labInit(vector<vector<int> > &lab, int n) {
	char ch; 
	for (int i = 0; i < n; ++i) 
		for (int j = 0; j < n; ++j) 
		{
			cin >> ch;
			if (ch == 'o')
				lab[i].push_back(0); // ������ �����
			else
				lab[i].push_back(-1);// �������
		}
	
	return lab;
}
//void retStep(vector<vector<int>> &lab, int n, pair<int, int> &from, pair<int, int> &to, vector<int> &dx, vector<int> &dy) {
//	vector<pair<int, int> > path;
//	do 
//	{
//		for (int k = 0; k < 4; ++k)
//		{
//			int kx = x + dx[k], ky = y + dy[k];
//
//		}
//
//	} while ();
//}

void waveAlgorithm(vector<vector<int>> &lab, int n, pair<int, int> &from, pair<int, int> &to) {
	queue<pair<int, int> > Q;
	pair<int, int> point; //���������� (x, y)
	int x, y, step = 1;
	vector<int> dx;    // = { 1, 0, -1, 0 }; //��������, ��������������� ������� ������ 
	vector<int> dy;   // = { 0, 1, 0, -1 }; //������, �����, �����, ������
	dx.push_back(1); dx.push_back(0); dx.push_back(-1); dx.push_back(0); // dx = { 1, 0, -1, 0 } 
	dy.push_back(0); dy.push_back(1); dy.push_back(0); dy.push_back(-1); // dy = { 0, 1, 0, -1 }
	lab[from.second][from.first] = step;
	Q.push(from);

	while (!Q.empty() && lab[to.second][to.first] == 0) //���� ������� �� ����� � �������� ������ �� ��������
	{
		point = Q.front();
		Q.pop();
		x = point.second;
		y = point.first;
		if (x != to.first && y != to.second) 
		{
			for (int k = 0; k < 4; ++k) //�������� �� ���� ������������ �������
			{ 
				int kx = x + dx[k], ky = y + dy[k];
				if (kx >= 0 && kx < n && ky >= 0 && ky < n && lab[ky][kx] == 0) //���� ����� �� �������
				{
					lab[ky][kx] = step + 1;
					//cout << lab[ky][kx];
					point.first = ky;
					point.second = kx;
					Q.push(point);
				}
			}
		}
		else //����� ������ ���������
		{
			int lenth = lab[to.first][to.second];
			x = to.second;
			y = to.first;
			step = lenth;
			vector<int> pathX;
			vector<int> pathY;
			do 
			{
				pathX[step] = x;
				pathY[step] = y;
				step--;
				for (int k = 0; k < 4; ++k)
				{
					int kx = x + dx[k], ky = y + dy[k];
					if (lab[ky][kx] == step && kx >= 0 && kx < n && ky >= 0 && ky < n)
					{
						x = x + dx[k];
						y = y + dy[k];
						cout << lab[y][x];
						break;
					//	y = ky;
					//	x = kx;
					//	lab[ky][kx] = lab[y][x];
					}
				}

			} while (step < 0); //while (y != from.first && x != from.second);
			pathX[0] = from.second;
			pathY[0] = from.first;
		}
		step++;
	}
}

void main() {
	freopen("labyrinth.in", "r", stdin);
	freopen("labyrinth.out", "w", stdout);
	int n;
	pair<int, int> from, to;
	cin >> n >> from.first >> from.second >> to.first >> to.second;
	vector<vector<int> > lab(n);
	lab = labInit(lab, n);
	waveAlgorithm(lab, n, from, to);
}