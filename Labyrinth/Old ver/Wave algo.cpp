#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <utility>

using namespace std;

vector<vector<int> > labInit(vector<vector<int> > &lab, int n) {
	char ch; 
	for (int i = 0; i < n; ++i) 
		for (int j = 0; j < n; ++j) 
		{
			cin >> ch;
			if (ch == 'o')
				lab[i].push_back(0);// = 0;
			else
				lab[i].push_back(-1);// [i][j] = -1;
		}
	
	return lab;
}

void waveAlgorithm(vector<vector<int>> &lab, int n, pair<int, int> &from, pair<int, int> &to) {
	queue<pair<int, int> > Q;
	pair<int, int> point; //���������� (x, y)
	int x, y, step = 1;
	vector<int> dx = { 1, 0, -1, 0 }; //��������, ��������������� ������� ������
	vector<int> dy = { 0, 1, 0, -1 }; //������, �����, �����, ������
	lab[from.second][from.first] = step;
	Q.push(from);

	while (!Q.empty() && lab[to.second][to.first] == 0) //���� ������� �� ����� � �������� ������ �� ��������
	{
		point = Q.front();
		Q.pop();
		x = point.first;
		y = point.second;
		if (x != to.first && y != to.second) 
		{
			for (int k = 0; k < 4; ++k) //�������� �� ���� ������������ �������
			{ 
				int kx = x + dx[k], ky = y + dy[k];
				if (kx >= 0 && kx < n && ky >= 0 && ky < n && lab[ky][kx] == 0) //���� ����� �� �������
				{
					lab[ky][kx] = step + 1;
					cout << lab[ky][kx];
					point.first = kx;
					point.second = ky;
					Q.push(point);
				}
			}
		}
		else 
		{
			cout << step;
			return;
			//����� ������ ���������
		}
		step++;
	}
}

void main() {
	freopen("labyrinth.in", "r", stdin);
	freopen("labyrinth.out", "w", stdout);
	int n;
	pair<int, int> from, to;
	cin >> n >> from.first >> from.second >> to.first >> to.second;
	vector<vector<int> > lab(n);
	lab = labInit(lab, n);
	waveAlgorithm(lab, n, from, to);
}