#include <vector>
#include <iostream>
#include <set>
using namespace std;

void dijkstra(vector<vector<pair<int, int> > > &adjList, int vertex) {
	set<int> visited;
	vector<int> signs(adjList.size(), -1);
	signs[vertex] = 0;
	while (visited.size() != adjList.size()) 
	{
		// ���� ������� � ����������� ������
		bool finded = false; // ������� ���� �� 1 ��������, ������������ �� �������
		int u;
		for (int i = 0; i < adjList.size(); i++) 
		{
			if (visited.find(i) == visited.end()) 
			{
				if (!finded) 
				{
					finded = true;
					u = i;
				}
				else if (signs[i] != -1 && signs[i] < signs[u]) 
				{
					u = i;
				}
			}
		}
		visited.insert(u);
		if (signs[u] == -1)
			continue;
		for (int i = 0; i < adjList[u].size(); i++) 
		{
			int index = adjList[u][i].first;
			if (visited.find(index) == visited.end()) 
			{
				int x = signs[u] + adjList[u][i].second;
				if (signs[index] == -1 || signs[index] > x)
					signs[index] = x;
			}
		}
	}
	for (int i = 0; i < signs.size(); i++)
		cout << signs[i] << endl;
}

int main() {
	freopen("input.in", "r", stdin);
	freopen("output.out", "w", stdout);
	int arcCount, vertexCount, vertex;
	cin >> arcCount >> vertexCount >> vertex;
	vector<vector<pair<int, int> > > adjList(vertexCount, vector<pair<int, int> >()); // ������ ����������
	for (int i = 0; i < arcCount; i++) 
	{
		int from, to, weight; // ����
		cin >> from >> to >> weight;
		adjList[from].push_back(make_pair(to, weight));
	}
	dijkstra(adjList, vertex);
	return 0;
}



