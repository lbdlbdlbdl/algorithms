#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <vector>
#include <stack>

using namespace std;

vector<int> graph[10];
int n;
stack<int> s;
int visited[10];

void topological_sort(int index) {
	visited[index] = 1;
	for (int i = 0; i < graph[index].size(); ++i) {
		if (!visited[graph[index][i]]) 
			topological_sort(graph[index][i]);
	}
	s.push(index);
}

void topological()
{
	for (int i = 0; i < n; ++i)
		visited[i] = 0;
	for (int i = 0; i < n; ++i)
		if (!visited[i])
			topological_sort(i);
	cout << "topological sort: ";
	while (!s.empty()) {
		cout << s.top() << " ";
		s.pop();
	}
}


void main() {
	freopen("input1.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int arcCount, vertexCount;
	cin >> arcCount >> vertexCount; // ���-�o ���, ���-�� ������
	n = vertexCount;
	for (int i = 0; i < arcCount; ++i) {
		int from, to; // ����
		cin >> from >> to;
		graph[from].push_back(to);
	}
	topological();
}