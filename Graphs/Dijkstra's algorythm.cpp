#define _CRT_SECURE_NO_WARNINGS
#include <vector>
#include <iostream>
#include <fstream>
#include <queue>
#define MAXN 10000

using namespace std;

vector<pair<int, int> > graph[MAXN];
int distMin[MAXN];
queue<int> q;
bool inQueue[MAXN];

void Dijkstra(int nodes) {
	for (int i = 2; i <= nodes; ++i)
		distMin[i] = MAXN;
	distMin[1] = 0;
	q.push(1);
	inQueue[1] = true;
	while (!q.empty()) 
	{
		int node = q.front();
		q.pop();
		inQueue[node] = false;
		for (vector<pair<int, int> >::iterator it = graph[node].begin(); it != graph[node].end(); ++it) 
		{
			if (distMin[it->first] > distMin[node] + it->second) 
			{
				distMin[it->first] = distMin[node] + it->second;
				if (!inQueue[it->first]) 
				{
					q.push(it->first);
					inQueue[it->first] = true;
				}
			}
		}
	}

	//print
	for (int i = 2; i <= nodes; ++i)
		cout << (distMin[i] < MAXN ? distMin[i] : 0);
	fclose(stdout);
}

void main() {
	freopen("input3.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int x, y, cost;
	int nodes, edges;
	cin >> nodes >> edges;
	while (edges--) 
	{
		cin >> x >> y >> cost;
		graph[x].push_back(make_pair(y, cost));
	}
	fclose(stdin);
	Dijkstra(nodes);
}