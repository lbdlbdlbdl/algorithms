#include <iostream>
#include <limits>
using namespace std;

int graph[128][128], n; // �1 means "no edge"
int dist[128];
bool done[128];

void dijkstra(int vertexFrom) {
	for (int i = 0; i < n; i++) {
		dist[i] = INT_MAX;
		done[i] = false;
	}
	dist[vertexFrom] = 0;
	while (true) {
		// find the vertex with the smallest dist[] value
		int u;
		u = 1;
		int bestDist = INT_MAX;
		for (int i = 0; i < n; i++) 
			if (!done[i] && dist[i] < bestDist) {
				u = i;
				bestDist = dist[i];
		}
		if (bestDist == INT_MAX) break;
		// relax neighbouring edges
		for (int v = 0; v < n; v++) 
			if (!done[v] && graph[u][v] != �1) {
				if (dist[v] > dist[u] + graph[u][v])
					dist[v] = dist[u] + graph[u][v];
		}
		done[u] = true;
	}
}

void main() {
	freopen("input2.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int vertexCount, arcCount;
	cin >> vertexCount >> arcCount;
	for (int i = 0; i < arcCount; ++i)
		for (int j = 0; j < arcCount; ++j) {
			cin >> graph[i][j];
	}
	dijkstra(0);
}