#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <vector>
#include <stack>

using namespace std;

bool dfs(vector<vector<int> > &adjList, int vertex, stack<int> &vertexes, vector<int> &colors) { // Depth First Search (����� � �������)
	if (colors[vertex] == 1)
		return true;
	if (colors[vertex] == 2)
		return false;
	colors[vertex] = 1;
	for (int i = 0; i < adjList.size(); ++i) {
		if (dfs(adjList, adjList[vertex][i], vertexes, colors))
			return true;
	}
	colors[vertex] = 2;
	vertexes.push(vertex);
	return false;
}

void topSort(vector<vector<int>> &adjList) {
	stack<int> vertexes; // ���� ��� ������
	vector<int> colors(adjList.size(), 0); // ������� ������, 0 - �����, 1 - �����, 2 - ������
	for (int i = 0; i < adjList.size(); ++i) {
		if (dfs(adjList, i, vertexes, colors))
			return; // ���� ���� �����������, �� �� ���������
	}

	// ����� ��������� ������ ������ ���������� � ������, ����� ����� ������� � ���������������
	vector<int> newOrder(adjList.size());
	for (int i = 0; i < adjList.size(); ++i) {
		newOrder[vertexes.top()] = i;
		vertexes.pop();
	}
	// ������� ��������������� ����
	for (int i = 0; i < adjList.size(); i++)
		for (int j = 0; j < adjList[i].size(); j++) {
			cout << newOrder[i] << ' ' << newOrder[adjList[i][j]] << endl;
	}
}

void main() {
	freopen("input1.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int arcCount, vertexCount;
	cin >> arcCount >> vertexCount; // ���-�o ���, ���-�� ������
	vector<vector<int>> adjList(vertexCount, vector<int>()); // ������ ����������
	for (int i = 0; i < arcCount; ++i) {
		int from, to; // ����
		cin >> from >> to;
		adjList[from].push_back(to);
	}
	topSort(adjList);
}