#include <fstream>
#include <iostream>
#include <string>

using namespace std;

int seekStringBM(string &s, string &q) {
	int k, i, j;
	int len_s = s.size(); //����� ������ s
	int len_q = q.size();
	//������ ������� �������
	int d[256]; 
	for (i = 0; i < 256; ++i)
		d[i] = len_q; //���������� len_q �� ���� ��������
	for (i = 0; i < len_q - 1; ++i) 
		d[q[i]] = len_q - i - 1;
	//�����
	i = len_q - 1;
	//j = i; //��������� ������ � �������
	while (i < len_s) {
		j = len_q - 1; // j - �� ������� 
		k = i; // k - �� ������
		while ((j >= 0) && (s[k] == q[j])) {
			--k;
			--j;
		}
		if (j < 0)
			return k + 1; //������ ���������� ���������
		i += d[s[i]];
	}
	return -1;
}
int main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	string s, q; 
	int res;
	getline(cin, s); //strlen(s) >= strlen(q)
	getline(cin, q); //����� ������ ��������� ������ q � ������ s
	res = seekStringBM(s, q);
	cout << res;
	return 0;
}
