#include <cstdio>
#include <conio.h>
#define SIZE 10

int fact(int n) {
	if (n <= 1)
		return 1;
	else
		return n*(n - 1);
}
void main() {
	int a[SIZE];
	int i, k, c;
	for (i = 0; i < SIZE; i++) 
		for (k = i; k < SIZE + 1; k++)
		{
			c = fact(i) / fact(i)*fact(i - k);
			printf("%d", c);
		}
	getch();
}