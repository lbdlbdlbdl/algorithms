#include <iostream>
#include <fstream>

using namespace std;

struct Tree {
	char word[256];
	Tree *left_child;
	Tree *right_child;
};

Tree *create() {
	Tree *T = new Tree;
	T->left_child = NULL;
	return T;
}
Tree *addNode(Tree *T, char *inputString) {
	strcpy(T->word, inputString);
	T->left_child = NULL;
	T->right_child = NULL;
	return T;
}

void clear(Tree *T) {
	if (T == NULL)
		return;
	clear(T->left_child);
	clear(T->right_child);
	delete T->word;
	delete T;
}

void search(Tree *T, char *inputWord) {
	if (strcmp(T->word, inputWord) > 0) //�����
	{
		if (T->left_child == NULL) 
		{
			addNode(T->left_child, inputWord);
		}
		else 
			search(T->left_child, inputWord);
	}
	else if (strcmp(T->word, inputWord) < 0) //������
	{
		if (T->right_child == NULL)
		{
			addNode(T->right_child, inputWord);
		}
		else
			search(T->right_child, inputWord);
	}
}

void printTree(Tree *T) {
	if (T->left_child != NULL)
		printTree(T->left_child);
	cout << T->left_child->word;
	if (T->right_child != NULL)
		printTree(T->right_child);
}

void main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	char inputWord[256];
	Tree *T = new Tree;
	cin >> inputWord;
	Tree *head = addNode(T, inputWord);
	while (!cin.eof())
	{
		cin >> inputWord; 
		search(head, inputWord);
	}
	printTree(head);

}