#include <stdio.h>
#include <string.h>

#define POSLE_ZAPYATOY 4

int pow(int x, int n)
{
    int result = 1;
    for (int i = 0; i < n; i++)
        result *= x;
    return result;
}

char symvol(int cifra)
{
    if (cifra <= 9)
	    return cifra + '0';
	else
	    return cifra + 'a' - 10;
}

int znach(char chislo) {
	if (chislo>='0' && chislo<='9')
		return chislo-'0';
	else 
	if (chislo>='A' && chislo<='Z')
		return chislo-'A'+10;
	else 
	if (chislo>='a' && chislo<='z')
		return chislo-'a'+10;
	else return -1;
}
int v_decyatich(char *chislo, int nach) {
	int razloj=0, stepen=1, i;
	for (i=strlen(chislo)-1; i>=0; --i) 
	{
		razloj+=znach(chislo[i])*stepen;
		stepen*=nach;
	}
return razloj;
}
void perevod(char *chislo, int nach, int konech, bool point) { //������� �� 10 �.�.
	int celaya;
	int x;
	int drobnaya = 0;
	if (!point)
	    celaya = v_decyatich(chislo, nach);
	else
	{
	    int len = strlen(chislo);
	    int point_pos = strchr(chislo, '.') - chislo; //������� �����
	    chislo[point_pos] = '\0';
	    celaya = v_decyatich(chislo, nach);
	    for (int i = 0; i < POSLE_ZAPYATOY; i++) // � ������� �������� ��������� ����� x/nach^POSLE_ZAPYATOY
	    {
	        if (i + 2 > len - point_pos)
	            break;
	        drobnaya += znach(chislo[point_pos + 1 + i]) * pow(nach, POSLE_ZAPYATOY - i - 1);
	    }
	    drobnaya = drobnaya  * pow(konech, POSLE_ZAPYATOY) / pow(nach, POSLE_ZAPYATOY); // ��������� ����� y/konech^POSLE_ZAPYATOY
	 }
	 int n = 0;
	 while (celaya / pow(konech, n) > 0)
	     n++;
	 for(int i = 0; i < n; i++)
	 {
	     x = pow(konech, n - i - 1);
	     chislo[i] = symvol(celaya / x);
	     celaya = celaya % x;

	 }
	 if (!point)
	     chislo[n] = '\0';
	 else
	 {
	     chislo[n] = '.';
	     for (int i = 0; i < POSLE_ZAPYATOY; i++)
	     {
	     	 x = pow(konech, POSLE_ZAPYATOY - i - 1);
	         chislo[i + n + 1] = symvol(drobnaya / x);
	         drobnaya = drobnaya % x;
	     }
	     chislo[n + 1 + POSLE_ZAPYATOY] = '\0';
	 }

}

int main() {
	FILE *in, *out;
	int nach=0, konech=0, b=1;
	bool point = false;
	char chislo[256];

	in = fopen("input.txt","r");
	out = fopen("output.txt","w");
	
	fscanf(in, "%d%s%d", &nach, chislo, &konech);

    int len = strlen(chislo);
    for (int i = 0; i < len; i++)
        if (chislo[i] == '.')
            point = true;
	perevod(chislo, nach, konech, point);
	fprintf(out, "%s", chislo);
	fclose(in);
	fclose(out);
	return 0;
}
