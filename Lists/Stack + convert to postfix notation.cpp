#include <iostream>
#include <fstream>
 
using namespace std;
 
struct tList {
	char data;
	tList *next;
};

struct Stack {
    struct tList *top;
};

Stack *create() {
	Stack *S = new Stack;
	S->top = NULL;
	return S;
}

int empty(Stack *S) {
	if (S->top == NULL)
		return 1;
	else return 0;
}

char top(Stack *S) { //������ ������� �������, �� �� ������� ���
	if (S->top)
		return S->top->data;
	else
	    return 0;
}

char pop(Stack *S) { //������ �������� �������� �������� ����� � ������� ���
	if (!empty(S))
	{
		tList *t = S->top;
		char ch = t->data;
		S->top = t->next;
		delete t;
		return ch;
	}
}

void push(Stack *S, char inputData) { //�������� � ���� ����� �������
	tList *t = new tList;
	t->data = inputData;
	t->next = S->top;
	S->top = t;
}

void polishNotation(Stack *S, char *inputString, char *outputString) {
	int j = 0; //��� ������� �� ouputString
	for (int i = 0; i < strlen(inputString); ++i) 
	{
		switch (inputString[i])
		{
		case '(':
			push(S, inputString[i]);
			break;
		case ')':
			if (!empty(S))
			{
				while (top(S) != '(')
					if (top(S))
						outputString[j++] = pop(S);
				pop(S);
			}
			break;
		case '*':
		case '/':
			if (!empty(S))
				while (top(S) == '*' || top(S) == '/')
					outputString[j++] = pop(S);
			push(S, inputString[i]);
			break;
		case '-':
		case '+':
			if (!empty(S))
				while (top(S) == '*' || top(S) == '/' || top(S) == '-' || top(S) == '+')
					outputString[j++] = pop(S);
			push(S, inputString[i]);
			break;
		default:
			outputString[j++] = inputString[i];
			break;
		}
	}
	while (top(S))
		outputString[j++] = pop(S);
	cout << outputString;

}

int main() {
/*
	Stack *S = create();
	push(S, '3');
	push(S, '2');
	cout << pop(S) << pop(S);
*/
	FILE *in = fopen("input.txt", "r");
    freopen("output.txt", "w", stdout);
	char inputS[256];
	char outputS[256];
	Stack *S = create();
	fscanf(in, "%s", inputS);
	polishNotation(S, inputS, outputS);
	fclose(in);	
    return 0;

}
