#include <iostream>
#include <fstream>
 
using namespace std;
 
struct tList {
    int data;
    struct tList *next;
};
 
tList *newElement(int n) {
    tList *p = new tList;
    p->data = n;
    p->next = NULL;
    return p;
}
 
void printList(tList *head) {
    tList *p = head;
    while (p)
    {
        cout << p->data << " ";
        p = p->next;
    }
}
 
tList *addElement(tList *head, int inputData) {
    tList *current = newElement(inputData);
    if (head == NULL)
        return current;
    if (current->data < head->data) //��������� � ������
    {
        current->next = head;
        head = current;
        return head;
    }
    else  //���� � ����
    {
        tList * p = head;
        while (p->next)
        {
            if (current->data < p->next->data)
            {
                current->next = p->next;
                p->next = current;
                return head;
            }
            p = p->next;
        }
        p->next = current; //���� ����� �������
    }
    return head;
}
 
int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    tList *head = NULL;
    int n; //������� �����
    while (cin >> n)
    {        
		head = addElement(head, n);
    }
    printList(head);
    return 0;
}